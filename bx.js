var crypto = require('crypto');

export default class bx {
    static transactions (apikey, secret) {
      var epoch = Math.floor((new Date()).getTime()/1000);
      var c = crypto.createHash('sha256');
      c.update(apikey + epoch + secret);
      var signature = c.digest().toString('hex');
      var formdata = new FormData();
      formdata.append('key', apikey);
      formdata.append('nonce', epoch);
      formdata.append('signature', signature);

      var promise = new Promise((resolve, reject)=>{
        fetch('https://bx.in.th/api/history/',{
          method:'POST',
          body:formdata
        }).then(r=>{
          return r.json();
        }).then(r=>{
          if(r.success) {
            r.transactions.sort((a,b)=>{
              return a.transaction_id - b.transaction_id;
            });
            resolve(r.transactions);
          }
          else {
            reject(r.error);
          }
        });
      });
      return promise;
    }
}
